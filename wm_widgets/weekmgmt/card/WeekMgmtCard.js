angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit,$state,$ionicActionSheet,$timeout){
	
	$scope.loadDayMgmtCardInfos = function(){
		var url = '/aeaiwm/services/WeekMgmt/rest/get-weekwork-info';
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.currentWeek = rspJson.currentWeek;
		});
	}
	$scope.loadDayMgmtCardInfos();

});