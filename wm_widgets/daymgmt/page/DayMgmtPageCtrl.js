angular.module('${menuCode}')
.filter('to_trusted', ['$sce', function ($sce) {
	return function (text) {
	    return $sce.trustAsHtml(text);
	};
}])
.controller("${widgetCode}Ctrl",function($scope,AppKit){
	$scope.setActiveTab = function (activeTab) {     
		　　$scope.activeTab = activeTab; 
	};
	
	$scope.initTabs = function(tabs){
		$scope.tabs = tabs;	
		$scope.activeTab = tabs[1];
	}	
	
	$scope.findDayWorkInfos = function(){
		var url = '/aeaiwm/services/DayMgmt/rest/find-daywork-infos';
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.past = rspJson.past;
			$scope.currentday = rspJson.currentday;
			$scope.follow = rspJson.follow;
			$scope.notes = rspJson.notes;
		});
	}
	$scope.findDayWorkInfos();
});