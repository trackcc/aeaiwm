angular.module('${menuCode}')
.controller("${widgetCode}Ctrl",function($scope,AppKit,$state,$ionicActionSheet,$timeout){
	
	$scope.loadDayMgmtCardInfos = function(){
		var url = '/aeaiwm/services/DayMgmt/rest/get-daywork-info';
		var promise = AppKit.getJsonApi(url);
		promise.success(function(rspJson){
			$scope.today = rspJson.today;
		});
	}
	$scope.loadDayMgmtCardInfos();

});