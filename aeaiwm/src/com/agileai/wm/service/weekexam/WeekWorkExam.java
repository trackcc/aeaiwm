package com.agileai.wm.service.weekexam;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/rest")
public interface WeekWorkExam {
    
    @GET
    @Path("/retrieve-weekexam-user-infos/{grpId}")  
    @Produces(MediaType.APPLICATION_JSON)
	public String retrieveWeekExamUserInfos(@PathParam("grpId") String grpId); 
    
    @GET
    @Path("/find-project-infos")  
    @Produces(MediaType.APPLICATION_JSON)
	public String findProjectInfos(); 
    
    @GET
    @Path("/retrieve-cursele-weekinfos/{weekTimeId}/{userId}/{grpId}")  
    @Produces(MediaType.APPLICATION_JSON)
	public String retrieveCurSeleWeekInfos(@PathParam("weekTimeId") String weekTimeId,@PathParam("userId") String userId,@PathParam("grpId") String grpId); 
    
    @GET
    @Path("/submit-curweek/{weekWorkId}")  
    @Produces(MediaType.TEXT_HTML)
	public String submitCurWeekWork(@PathParam("weekWorkId") String weekWorkId); 
    
    @GET
    @Path("/show-submit-btn/{grpId}/{userCode}")  
    @Produces(MediaType.APPLICATION_JSON)
	public String showSubmitBtn(@PathParam("grpId") String grpId,@PathParam("userCode") String userCode); 
    
    @GET
    @Path("/retrieve-weekwork-infos/{weekWorkId}/{userId}/{grpId}")  
    @Produces(MediaType.APPLICATION_JSON)
	public String retrieveWeekworkInfos(@PathParam("weekWorkId") String weekWorkId,@PathParam("userId") String userId,@PathParam("grpId") String grpId);
   
    @GET
    @Path("/sync-user-projectId/{grpId}")  
    @Produces(MediaType.TEXT_HTML)
	public String syncUserProjectId(@PathParam("grpId") String grpId); 
}
