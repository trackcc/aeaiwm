package com.agileai.wm.service.dayexam;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/rest")
public interface DayWorkExam {
    
    @GET
    @Path("/retrieve-dayexam-user-infos/{grpId}")  
    @Produces(MediaType.APPLICATION_JSON)
	public String retrieveDayExamUserInfos(@PathParam("grpId") String grpId); 
    
    @GET
    @Path("/find-daywork-infos/{userId}/{grpId}")  
    @Produces(MediaType.APPLICATION_JSON)
	public String findDayWorkInfos(@PathParam("userId") String userId,@PathParam("grpId") String grpId); 
    
    @GET
    @Path("/find-prodayworklist-infos/{userId}/{startTime}/{grpId}")  
    @Produces(MediaType.APPLICATION_JSON)
	public String findProDayWorkListInfos(@PathParam("userId") String userId,@PathParam("startTime") String startTime,@PathParam("grpId") String grpId);     
    
    @GET
    @Path("/find-lastdayworklist-infos/{userId}/{startTime}/{grpId}")  
    @Produces(MediaType.APPLICATION_JSON)
	public String findLastDayWorkListInfos(@PathParam("userId") String userId,@PathParam("startTime") String startTime,@PathParam("grpId") String grpId);  
    
    @GET
    @Path("/find-project-infos")  
    @Produces(MediaType.APPLICATION_JSON)
	public String findProjectInfos(); 
    
    @GET
    @Path("/get-active-userId/{userCode}")  
    @Produces(MediaType.APPLICATION_JSON)
	public String getActiveUserId(@PathParam("userCode") String userCode); 
    
    @GET
    @Path("/retrieve-cursele-dayinfos/{userId}/{startTime}/{grpId}")  
    @Produces(MediaType.APPLICATION_JSON)
	public String retrieveCurSeleDayInfos(@PathParam("userId") String userId,@PathParam("startTime") String startTime,@PathParam("grpId") String grpId); 
    
    @GET
    @Path("/sync-user-projectId/{grpId}")  
    @Produces(MediaType.TEXT_HTML)
	public String syncUserProjectId(@PathParam("grpId") String grpId); 
}
