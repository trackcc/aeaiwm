package com.agileai.wm.service.weektime;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.agileai.wm.service.model.WeekTimeModel;

@Path("/rest")
public interface WeekTime {
	
    @GET
    @Path("/find-weektimelist-infos")  
    @Produces(MediaType.APPLICATION_JSON)
	public String findWeekTimeListInfos(); 
    
    @GET
    @Path("/get-week-time/{weekTimeId}")  
    @Produces(MediaType.APPLICATION_JSON)
	public String getWeekTime(@PathParam("weekTimeId") String weekTimeId); 
    
    @GET
    @Path("/del-week-time/{weekTimeId}")  
    @Produces(MediaType.TEXT_HTML)
	public String delWeekTime(@PathParam("weekTimeId") String weekTimeId);
    
    @GET
    @Path("/calculate-stand-day/{stime}/{etime}")  
    @Produces(MediaType.TEXT_HTML)
	public String calculateStandDay(@PathParam("stime") String stime,@PathParam("etime") String etime);
    
    @POST
    @Path("/save-week-time")  
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_HTML)
	public String saveWeekTime(WeekTimeModel model);
}
