package com.agileai.wm.module.workgroup.handler;

import java.util.*;

import com.agileai.domain.*;
import com.agileai.hotweb.controller.core.TreeSelectHandler;
import com.agileai.hotweb.domain.*;
import com.agileai.wm.module.workgroup.service.WmGroupTreeManage;

public class WmGroupParentSelectHandler
        extends TreeSelectHandler {
    public WmGroupParentSelectHandler() {
        super();
        this.serviceId = buildServiceId(WmGroupTreeManage.class);
        this.isMuilSelect = false;
    }

    protected TreeBuilder provideTreeBuilder(DataParam param) {
        List<DataRow> records = getService().queryPickTreeRecords(param);
        TreeBuilder treeBuilder = new TreeBuilder(records, "GRP_ID",
                                                  "GRP_NAME", "GRP_PID");

        String excludeId = param.get("GRP_ID");
        treeBuilder.getExcludeIds().add(excludeId);

        return treeBuilder;
    }

    protected WmGroupTreeManage getService() {
        return (WmGroupTreeManage) this.lookupService(this.getServiceId());
    }
}
