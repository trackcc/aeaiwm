package com.agileai.wm.module.weektime.handler;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.controller.core.PickFillModelHandler;
import com.agileai.wm.module.weektime.service.WeekTimeListSelect;

public class WeekTimeListSelectListHandler
        extends PickFillModelHandler {
    public WeekTimeListSelectListHandler() {
        super();
        this.serviceId = buildServiceId(WeekTimeListSelect.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    protected void initParameters(DataParam param) {
        initParamItem(param, "WT_BEGIN", "");
        initParamItem(param, "WT_END", "");
        initParamItem(param, "WT_ID", "");
    }

    protected WeekTimeListSelect getService() {
        return (WeekTimeListSelect) this.lookupService(this.getServiceId());
    }
    
   
}
