package com.agileai.wm.module.weektime.handler;

import java.util.Date;

import com.agileai.domain.DataParam;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.wm.cxmodule.WmWeektimeManage;

public class WmWeektimeManageEditHandler
        extends StandardEditHandler {
    public WmWeektimeManageEditHandler() {
        super();
        this.listHandlerClass = WmWeektimeManageListHandler.class;
        this.serviceId = buildServiceId(WmWeektimeManage.class);
    }

    protected void processPageAttributes(DataParam param) {
    }

    @PageAction
    public ViewRenderer calculateStandDay(DataParam param){
    	String responseText = FAIL;
    	try {
    		this.setAttributes(param);
        	String startTimeStr = param.get("WT_BEGIN");
        	Date startTime = DateUtil.getDateTime(startTimeStr);
        	String endTimeStr = param.get("WT_END");
        	Date endTime = DateUtil.getDateTime(endTimeStr);
    	    long day=endTime.getTime()-startTime.getTime();
    	    long wtStandDay=day/(24*60*60*1000)+1;
    		processPageAttributes(param);
    		responseText = String.valueOf(wtStandDay);
		} catch (Exception e) {
			log.error(e.getLocalizedMessage(), e);
		}
		return new AjaxRenderer(responseText);
  	}
    
    
    protected WmWeektimeManage getService() {
        return (WmWeektimeManage) this.lookupService(this.getServiceId());
    }
}

