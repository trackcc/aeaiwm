package com.agileai.wm.module.weektime.service;

import com.agileai.hotweb.bizmoduler.core.PickFillModelServiceImpl;
import com.agileai.wm.module.weektime.service.WeekTimeListSelect;

public class WeekTimeListSelectImpl
        extends PickFillModelServiceImpl
        implements WeekTimeListSelect {
    public WeekTimeListSelectImpl() {
        super();
    }
}
