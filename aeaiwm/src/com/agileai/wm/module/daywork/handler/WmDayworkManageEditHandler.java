package com.agileai.wm.module.daywork.handler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import com.agileai.domain.DataParam;
import com.agileai.domain.DataRow;
import com.agileai.hotweb.annotation.PageAction;
import com.agileai.hotweb.controller.core.StandardEditHandler;
import com.agileai.hotweb.domain.FormSelectFactory;
import com.agileai.hotweb.domain.core.User;
import com.agileai.hotweb.renders.AjaxRenderer;
import com.agileai.hotweb.renders.ViewRenderer;
import com.agileai.util.DateUtil;
import com.agileai.wm.cxmodule.WmDayworkManage;
import com.agileai.wm.module.daywork.service.WmNoteManage;

public class WmDayworkManageEditHandler
        extends StandardEditHandler {
    public WmDayworkManageEditHandler() {
        super();
        this.listHandlerClass = WmDayworkManageListHandler.class;
        this.serviceId = buildServiceId(WmDayworkManage.class);
    }
    protected void processPageAttributes(DataParam param) {
        setAttribute("TW_ENV",
                     FormSelectFactory.create("TW_ENV_TYPE")
                                      .addSelectedValue(getAttributeValue("TW_ENV","InOffice")));
        
        User user = (User)this.getUser();
        String currentUserId = user.getUserId();
        this.setAttribute("USER_ID", currentUserId);
        DataRow dataRow = getService().getLastDayRecord(currentUserId);
        if (dataRow != null && dataRow.size() > 0){
            Date tempDay = new Date(dataRow.getTimestamp("LAST_DAY").getTime());
            Date nextDay = DateUtil.getDateAdd(tempDay, DateUtil.DAY, 1);//将tempDay 进行加一处理
            this.setAttribute("TW_TIME", DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,nextDay));//将日期格式化        	
        }else{
        	this.setAttribute("TW_TIME", DateUtil.getDateByType(DateUtil.YYMMDD_HORIZONTAL,new Date()));//将日期格式化
        }
    }
   
  @PageAction
  public ViewRenderer savaDayWork(DataParam param){
	  String responseText="true";
    	try {
    		
    		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        	Date  date = new Date(); 
        	String TW_TIME = param.getString("TW_TIME");
			if(sdf.parse(sdf.format(date)).getTime()>sdf.parse(TW_TIME).getTime()){
				responseText="不能新增今天以前的日报，请重新选择日期！！";
			}else{
				if(param.get("daywork_note").equals("0")){
		    		String operateType = param.get(OperaType.KEY);
		    		if (OperaType.CREATE.equals(operateType)){
		    			List<DataRow> list = getService().findRecords(param);
		    			if(list.size()==0){
		    				param.put("TW_CONTENT","<ol><li>工作项目1</li><li>工作项目2</li></ol>");
		    				getService().createRecord(param);	
		    			}else{
		    				responseText="当前选择日期日报已存在，无需重复新增！！！！";
		    			}
		    			
		    		}
		    		else if (OperaType.UPDATE.equals(operateType)){
		    			getService().updateRecord(param);	
		    		}
		    	}else if (param.get("daywork_note").equals("1")){
		    		String operateType = param.get(OperaType.KEY);
		    		WmNoteManage wmNoteManage = lookupService(WmNoteManage.class);
		    		if (OperaType.CREATE.equals(operateType)){
		    			param.put("NOTE_DESCRIBE","<ol><li>备忘项目1</li><li>备忘项目2</li></ol>");
		    			wmNoteManage.createRecord(param);	
		    		}
		    		else if (OperaType.UPDATE.equals(operateType)){
		    			wmNoteManage.updateRecord(param);	
		    		}
				}
			}
		
		} catch (ParseException e) {
			e.printStackTrace();
		}
    	return new AjaxRenderer(responseText);
    	
	}
    protected WmDayworkManage getService() {
        return (WmDayworkManage) this.lookupService(this.getServiceId());
    }
   
}
