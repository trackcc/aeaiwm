<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ page contentType="text/html;charset=UTF-8"%>
<jsp:useBean id="pageBean" scope="request" class="com.agileai.hotweb.domain.PageBean"/>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title>关联属性编辑</title>
<%@include file="/jsp/inc/resource.inc.jsp"%>
<script type="text/javascript">
function updateRelProperties(){
	if(validate()){
		postRequest('form1',{actionType:'updateRelProperties',showSplash:true,onComplete:function(responseText){
			hideSplash();
			if(responseText == 'success'){
				parent.PopupBox.closeCurrent();
				parent.reloadPage();
			}else{
				writeErrorMsg("保存出错啦！");
			}
		}});
	}
}
</script>
</head>
<body>
<form action="<%=pageBean.getHandlerURL()%>" name="form1" id="form1" method="post">
<%@include file="/jsp/inc/message.inc.jsp"%>
<div id="__ParamBar__" style="float: right;">&nbsp;</div>
<div id="__ToolBar__">
<table border="0" cellpadding="0" cellspacing="1">
<tr>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="updateRelProperties();"><input value="&nbsp;" type="button" class="saveImgBtn" id="saveImgBtn" title="保存" />保存</td>
   <td onmouseover="onMover(this);" onmouseout="onMout(this);" class="bartdx" align="center" onclick="javascript:parent.PopupBox.closeCurrent();"><input value="&nbsp;" type="button" class="closeImgBtn" title="关闭" />关闭</td>
</tr>
</table>
</div>
<table class="detailTable" cellspacing="0" cellpadding="0">
<tr>
	<th width="100" nowrap>编码</th>
	<td><input name="USER_CODE" type="text" class="text" id="USER_CODE" value="<%=pageBean.inputValue("USER_CODE")%>" size="24" readonly="readonly" label="编码" />
</td>
</tr>
<tr>
	<th width="100" nowrap>名称</th>
	<td><input name="USER_NAME" type="text" class="text" id="USER_NAME" value="<%=pageBean.inputValue("USER_NAME")%>" size="24" readonly="readonly" label="名称" />
</td>
</tr>
<tr>
	<th width="100" nowrap>岗位</th>
	<td><select id="EMP_JOB" label="岗位" name="EMP_JOB" class="select"><%=pageBean.selectValue("EMP_JOB")%></select>
</td>
</tr>
<tr>
	<th width="100" nowrap>排序</th>
	<td><input id="EMP_SORT" label="排序" name="EMP_SORT" type="text" value="<%=pageBean.inputValue("EMP_SORT")%>" size="24" class="text" />
</td>
</tr>
</table>
<input type="hidden" id="GRP_ID" name="GRP_ID" value="<%=pageBean.inputValue("GRP_ID")%>" />
<input type="hidden" id="USER_ID" name="USER_ID" value="<%=pageBean.inputValue("USER_ID")%>" />
<input type="hidden" name="actionType" id="actionType" value=""/>
</form>
<script language="javascript">
requiredValidator.add("EMP_JOB");
requiredValidator.add("EMP_SORT");
intValidator.add("EMP_SORT");
</script>
</body>
</html>
<%@include file="/jsp/inc/scripts.inc.jsp"%>
